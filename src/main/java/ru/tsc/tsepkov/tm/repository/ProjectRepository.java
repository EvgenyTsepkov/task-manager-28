package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.repository.IProjectRepository;
import ru.tsc.tsepkov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
