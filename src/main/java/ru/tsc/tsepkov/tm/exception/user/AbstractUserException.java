package ru.tsc.tsepkov.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull String message) {
        super(message);
    }

    public AbstractUserException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull String message,
            @NotNull Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
